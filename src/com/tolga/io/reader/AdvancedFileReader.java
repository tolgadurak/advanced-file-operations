package com.tolga.io.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;


import com.tolga.io.exception.NegativeSeekOffSetException;

public final class AdvancedFileReader extends FileReader implements ReaderAdapter {

	public AdvancedFileReader(File file) throws FileNotFoundException {
		super(file);
		randomAccessFile = new RandomAccessFile(file, "r");
		bufReader = new BufferedReader(new FileReader(file));
	}

	@Override
	public int readByChar() throws IOException {
		return this.read();
	}
	BufferedReader bufReader ;
	@Override
	public String readByLine() throws IOException {
		String str = bufReader.readLine();
		if(str == null) {
			bufReader.close();
		}
		return str;
	}

	private RandomAccessFile randomAccessFile;

	@Override
	public byte readReverseByChar() throws IOException, NegativeSeekOffSetException {
		long randomAccessPosition = 0;
		if (randomAccessFile.getFilePointer() == 0)
			randomAccessPosition = randomAccessFile.length() - 1;
		else
			randomAccessPosition = randomAccessFile.getFilePointer() - 2;
		if(randomAccessPosition < 0) {
			randomAccessFile.seek(0);
			return -1;
		}
		randomAccessFile.seek(randomAccessPosition);
		
		return randomAccessFile.readByte();
	}

	
	

}
