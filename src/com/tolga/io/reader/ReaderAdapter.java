package com.tolga.io.reader;

import java.io.IOException;

import com.tolga.io.exception.NegativeSeekOffSetException;

public interface ReaderAdapter {
	int readByChar() throws IOException;
	String readByLine() throws IOException;
	byte readReverseByChar() throws IOException, NegativeSeekOffSetException;
}
