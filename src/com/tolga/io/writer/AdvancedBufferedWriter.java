package com.tolga.io.writer;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Serializable;
import java.io.Writer;
import java.util.List;

public final class AdvancedBufferedWriter extends BufferedWriter implements WriterAdapter, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AdvancedBufferedWriter(Writer out) {
		super(out);		
		// TODO Auto-generated constructor stub
	}

	@Override
	public void writeArrayList(List<Object> list) throws IOException {
		for(Object obj : list) {
			super.write(obj.toString());
			super.flush();
		}
	}

}
