package com.tolga.io.writer;

import java.io.IOException;
import java.util.List;

public interface WriterAdapter {
	public void writeArrayList(List<Object> list) throws IOException;
}
