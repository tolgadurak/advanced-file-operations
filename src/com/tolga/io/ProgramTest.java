package com.tolga.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;


import com.tolga.io.exception.NegativeSeekOffSetException;
import com.tolga.io.reader.AdvancedFileReader;


public class ProgramTest {
	private File getFile(String fileName) {
		
		File file = new File(this.getClass().getResource(fileName).getFile());
		return file;

	}

	public static void main(String[] args) {
		ProgramTest test = new ProgramTest();
		File file = test.getFile("input.txt");
		try {
			AdvancedFileReader reader = new AdvancedFileReader(file);

			// Straightforward Reading by char
			while (true) {
				int charInt = reader.readByChar();
				if (charInt == -1)
					break;
				char current = (char) charInt;
				System.out.print(current);
			}
			System.out.println();
			while (true) {
				int charInt = reader.readReverseByChar();
				if (charInt == -1)
					break;
				char current = (char) charInt;
				System.out.print(current);
			}
			System.out.println();
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NegativeSeekOffSetException e) {
			return;
		}
	}

}
