package com.tolga.io.exception;

public class NegativeSeekOffSetException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NegativeSeekOffSetException(String message) {
		super(message);
	}
}
